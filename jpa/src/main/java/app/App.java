package app;

import domain.Book;
import jakarta.persistence.Persistence;

public class App {
    public static void main(String[] args) {
        var factory = Persistence
                .createEntityManagerFactory("at.htlstp.jpa.books");
        var entityManager = factory.createEntityManager();
    }
}
