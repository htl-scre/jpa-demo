create sequence books_seq as bigint;
create sequence authors_seq as bigint;

CREATE TABLE publishers
(
    publisher_name VARCHAR(255) NOT NULL,
    CONSTRAINT pk_publishers PRIMARY KEY (publisher_name)
);

CREATE TABLE authors
(
    id          BIGINT NOT NULL,
    author_name VARCHAR(255),
    CONSTRAINT pk_authors PRIMARY KEY (id)
);

CREATE TABLE books
(
    id        BIGINT NOT NULL,
    title     VARCHAR(255),
    publisher VARCHAR(255),
    CONSTRAINT pk_books PRIMARY KEY (id)
);

CREATE TABLE books_authors
(
    book_id   INTEGER NOT NULL,
    author_id INTEGER NOT NULL,
    CONSTRAINT pk_books_authors PRIMARY KEY (book_id, author_id)
);

ALTER TABLE books
    ADD CONSTRAINT FK_BOOKS_ON_PUBLISHER_NAME FOREIGN KEY (publisher) REFERENCES publishers (publisher_name);

ALTER TABLE books_authors
    ADD CONSTRAINT fk_booaut_on_author FOREIGN KEY (author_id) REFERENCES authors (id);

ALTER TABLE books_authors
    ADD CONSTRAINT fk_booaut_on_book FOREIGN KEY (Book_id) REFERENCES books (id);

alter sequence books_seq owner to unterricht;
alter sequence books_seq owned by books.id;
alter sequence authors_seq owner to unterricht;
alter sequence authors_seq owned by authors.id;

insert into publishers(publisher_name)
values ('Penguin Publishing'),
       ('Harper Collins'),
       ('Simon & Schuster');

insert into authors(id, author_name)
values (nextval('authors_seq'), 'Frank Herbert'),
       (nextval('authors_seq'), 'Bill Ransom'),
       (nextval('authors_seq'), 'Robert Martin');

insert into books(id, title, publisher)
values (nextval('books_seq'), 'Clean Code', 'Penguin Publishing'),
       (nextval('books_seq'), 'The Jesus Incident', 'Penguin Publishing'),
       (nextval('books_seq'), 'Dune', 'Harper Collins'),
       (nextval('books_seq'), 'Dune Messiah', 'Simon & Schuster'),
       (nextval('books_seq'), 'Burn', 'Harper Collins');

insert into books_authors(book_id, author_id)
values (1, 3),
       (2, 1),
       (2, 2),
       (3, 1),
       (4, 1),
       (5, 2);

/* hibernate performance */
alter sequence books_seq increment by 50;
alter sequence authors_seq increment by 50;